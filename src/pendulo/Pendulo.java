/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pendulo;

/**
 *
 * @author Ma
 */


import java.awt.*;
import javax.swing.*;
 
public class Pendulo extends JPanel implements Runnable {
 
  private double angulo = (50);
  private int largo;
 
  public Pendulo(int largo) {
    this.largo = largo;
    
  }
 
  @Override
  public void paint(Graphics g) {
    g.setColor(Color.WHITE);
    g.fillRect(0, 0, 500, 500);
    g.setColor(Color.BLUE);
    int cuerdax = 100; 
    int cuerday = 2;
    int pesox = (int) ((cuerdax) + (Math.sin(angulo) * largo));
    int pesoy = (int) ((cuerday) + ((Math.cos(angulo) * largo)));
    int torsox= pesox+(int) ((Math.sin(angulo) * largo));
    int torsoy= pesoy+(int) ((Math.cos(angulo) * largo));
    int brazox= 42+(int) ((Math.sin(angulo) * (largo)));
    int brazoy= 30+(int) ((Math.cos(angulo) * (largo)));
    
    int brazo2x= 160+(int) ((Math.sin(angulo) * (largo)));
    int brazo2y= 30+(int) ((Math.cos(angulo) * (largo)));
    int pie1x= 42+(int) ((Math.sin(angulo) * (largo*2)));
    int pie1y= (130+(int) ((Math.cos(angulo) * (largo))));
    
    g.drawLine(cuerdax, cuerday, pesox, pesoy);
    g.fillOval(pesox-15, pesoy-15, 30, 30);
    g.drawLine(pesox, pesoy, torsox, torsoy);
    g.drawLine(pesox, pesoy, brazox, brazoy);
    g.drawLine(torsox, torsoy, pie1x, pie1y);
    
    g.drawLine(pesox, pesoy, brazo2x, brazo2y);
    g.drawLine(torsox, torsoy, pie1x+118, pie1y);

    
  
    
    
  }
 
  @Override
  public void run() {
    double anguloAc, velAng = 0;
    while (true) {
      anguloAc = -9.81 / largo * Math.sin(angulo);
      velAng += anguloAc * 0.1;
      angulo += velAng * 0.1;
      repaint();
      try {
        Thread.sleep(15);
      } catch (InterruptedException ex) {
      }
    }
 }
  
 
  @Override
  public Dimension getPreferredSize() {
    return new Dimension(250, 200);
  }
 
  public static void main(String[] args) {
    JFrame.setDefaultLookAndFeelDecorated(true);
    
    Pendulo p = new Pendulo(80);
    JFrame f = new JFrame("Pendulo");
    f.add(p);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.pack();
    f.setVisible(true);

    new Thread(p).start();
  }
}

